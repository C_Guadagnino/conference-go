from django.apps import AppConfig


print("Did this restart?")


class EventsConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "events"
