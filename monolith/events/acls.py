from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import json
import requests


def get_location_photo(search):
    url = "https://api.pexels.com/v1/search?query=" + search
    headers = {"Authorization": PEXELS_API_KEY}

    response = requests.get(url, headers=headers)

    data = json.loads(response.content)
    location_photo = data["photos"]
    if location_photo and len(location_photo) > 0:
        return location_photo


def get_weather(city, state):
    url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},&appid={OPEN_WEATHER_API_KEY}"

    response = requests.get(url)

    data = json.loads(response.content)[0]
    lon = data["lon"]
    lat = data["lat"]

    weather_url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}&units=imperial"
    response = requests.get(weather_url)

    w_data = json.loads(response.content)
    temp = w_data["main"]["temp"]
    description = w_data["weather"][0]["description"]
    weather = {
        "temperature": temp,
        "description": description,
    }
    return weather
